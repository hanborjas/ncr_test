function solution(A, B, C) {
    let inicio = 0;
    let fin = C.length - 1;
    let suma = -1; //clavos acumulados
    while (inicio <= fin) {
        let mid = parseInt((inicio + fin) / 2); //por cada plancha son dos clavos
        if (check(A, B, C, mid+1)) { //chequeo que, dados los array, saber cuál es el número mínimo posible para aclavar todos
            fin = mid - 1;
            suma = mid+1;
        } else {
            inicio = mid + 1;
        }
    }
    return suma;
}
//comprueba de la función
function check(a, b, c, num) {
    let clavados = new Array(2*c.length + 1).fill(0);

    for (var i = 0; i < num; ++i) {
        ++clavados[c[i]]; //por cada índice le añado uno
    }
    for (i = 1; i < clavados.length; ++i) {
        clavados[i] += clavados[i-1]; //recorro el array de clavados para saber cuáles ya están usados
    }
    for (i = 0; i < a.length; ++i) {
        if (clavados[b[i]] <= clavados[a[i]-1]) return false;
    }
    return true;
}

