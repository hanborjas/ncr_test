//primero comparamos los dos array
const comparacion = (palabras1, palabras2) => {
    for (let i = 0; i < palabras1.length; i++) {
      if (palabras1[i] !== palabras2[i]) {
        return false;
      }
    }

    return true;
};

//buscamos el largo del string y del anagrama
const encontrarAnagramas = (str, ptr) => {
  const S = str.length;
  const P = ptr.length;
  if(S < P){
      return [];
  }

  //declaramos dos array vacíos
  const sCuenta = new Array(256).fill(0);
  const pCuenta = new Array(256).fill(0);

  //Array para guardar coincidencias que consiga
  const contador= [];


  for (let i = 0; i < P; i++) {
    sCuenta[s[i].charCodeAt()]++;
    pCuenta[p[i].charCodeAt()]++;
  }

  //se obtiene la subcadena de longitud P  al quitar el primer carácter, moviéndolo y luego añades al contador
  for (let i = P; i < S; i++) {
    if (comparacion(sCuenta, pCuenta)) {
        contador.push(i - P);
    }

    //añade el siguiente carácter a la cuenta
    sCuenta[s[i].charCodeAt()]++;

    //Quita el primer carácter de la cuenta
    sCuenta[s[i - P].charCodeAt()]--;
  }

  //comparación con la subcadena
  if (comparacion(sCuenta, pCuenta)) {
    contador.push(S - P);
  }

  //devuelve el resultado del contador
  return contador;
};
